export interface Repository {
    html_url: string;
    name: string;
    description: string;
    watchers: number;
    forks: number;
}