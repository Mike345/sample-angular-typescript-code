export interface User {
    name: string;
    avatar_url: string;
    html_url: string;
    public_repos: number;
    public_gists: number;
    followers: number;
    following: number;
    login: string;
    location: string;
    email: string;
    blog: string;
    created_at: string;
}