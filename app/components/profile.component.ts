import {Component} from '@angular/core';
import {GitHubService} from '../services/github.service';
import {User} from '../models/User';
import {Repository} from "../models/Repository";

@Component({
    moduleId: module.id,
    selector: 'profile',
    templateUrl: 'profile.component.html',
})
export class ProfileComponent {
    public user: User;
    public repositories: Array<Repository>;
    public username: string;
    private errorMessage: string;

    /**
     * ProfileComponent constructor
     * @param _gitHubService
     */
    public constructor(private _gitHubService: GitHubService) {
        this.user = null;
    }

    /**
     * Search username on GitHub
     */
    public searchUser(): void {
        this.errorMessage = null;

        /** try to find the user */
        this._gitHubService.getUser(this.username).subscribe(
            user => {
                this.user = user;
            },
            error => {
                this.errorMessage = error
            }
        );

        /** try to find the user repositories */
        this._gitHubService.getRepositories(this.username).subscribe(
            repositories => {
                this.repositories = repositories;
            },
            error => {
                this.errorMessage = error
            }
        );
    }
}
