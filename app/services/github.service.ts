import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from "rxjs";
import {User} from "../models/User";
import {Repository} from "../models/Repository";
import 'rxjs/add/operator/map';

@Injectable()
export class GitHubService {
    private _gitHubUsersApiUrl = 'http://api.github.com/users/';
    private _clientId: string = 'xxxxxx'; // removed for security reasons
    private _clientSecret: string = 'xxxxxx'; // removed for security reasons

    /**
     * GitHubService constructor
     * @param _http
     */
    public constructor(private _http: Http) { }

    /**
     * @returns {Observable<User>}
     */
    public getUser(username: string): Observable<User> {
        return this._http.get(this._gitHubUsersApiUrl + username + '?client_id=' +
            this._clientId + '&client_secret=' + this._clientSecret).map(res => res.json());
    }

    /**
     * @returns {Observable<Array<Repository>>}
     */
    public getRepositories(username: string): Observable<Array<Repository>> {
        return this._http.get(this._gitHubUsersApiUrl + username + '/repos?client_id=' +
            this._clientId + '&client_secret=' + this._clientSecret).map(res => res.json());
    }
}
